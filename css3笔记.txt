css3相关内容


1.实现元素的变换效果：transform
	transform-style:flat(默认：2d效果) | preserve-3d（元素呈现3D效果）
	transform下的属性：rotate(旋转角度)			skew(扭曲角度)			scale(x,y)//图形缩放			translate(x,y)//平移

2.transition(平滑过渡)原理：该属性本身会将元素属性的变化包含进去。所以可以最开始就声明。当其所操作的属性发生变化时，都可以检测到


3.实现元素的动画效果：animation（定义动画）    配合@keyframes（用来定义动画的一系列动作）一起使用
			animation-play-state:	running | paused	//定义动画的播放状态
	

4.渐变色：
	线性渐变（linear）:linear-gradient(to bottom,#fff,#999)		第一个参数为渐变方向，后面参数为渐变的颜色
	
	
5.页面自动换行：
	word-break:break-all  //会把单词截断，以固定长度进行换行
	word-wrap:break-word  //不会把单词截断，会把单词当作一个整体，当宽度不够放下整个单词时，会把单词整体放到下一行
	white-space:normal		//使用该属性，为了实现兼容性
	white-space:nowrap; overflow:hidden; text-overflow:ellipsis;      //实现中文的自动换行，同时超出边框宽度的部分会用...代替
		

6.@font-face：自定义字体
	@font-face{				//定以后，就可以在font-family中使用该字体
		font-family:字体名称；
		src:字体的路径;
	}
	
	
7.background相关：
	background-origin:设置背景图片的原始起始位置
		background-origin ： border-box | padding-box | content-box;
		参数分别表示背景图片是从边框，还是内边距（默认值），或者是内容区域开始显示。
	background-clip:对背景图片做裁剪
	background-size:用来设置背景图的大小
		background-size: auto | <长度值> | <百分比> | cover | contain
		设置为cover意思为覆盖，即将背景图片等比缩放以填满整个容器；
		设置为contain意思为容纳，即将背景图片等比缩放至某一边紧贴容器边缘为止。
	多图片背景：background-image定义多个url，然后通过background-position来为每个图片定义位置，否则三个会重叠
	
7.多列布局属性：
	columns属性：columns：<column-width> || <column-count>		//每列的宽度，列数
				//使用该属性可实现类似报纸杂志的多列显示效果
				
				column-gap: normal || <length>		//定义列间距，默认值为1em
				
				column-rule: <column-rule-width>|<column-rule-style>|<column-rule-color>
				//用来定义列与列间的边框，为了能更清晰的区分列，所定义的边框不占用空间位置
				
				column-span: none | all		//不夸列 | 横跨所有列
				
				box-sizing:对盒子模型的重新定义，可以在自适应布局中让布局正常显示
				

8.媒体类型
	@media：根据不同屏幕的分辨率加载不同的样式
		@media 媒体类型and （媒体特性）{你的样式}
		例：@media screen and (max-width:480px){
				.ads {
					display:none;
				}
			}
			
		<link rel="stylesheet" media="screen and (max-device-width:480px)" href="iphone.css" />		//用于加载不同的css
		

10.响应式设计：
	使用meta标签定义可视区域：<meta name=”viewport” content=”width=device-width,initial-scale=1.0” />

	
11.css的外轮廓属性：
	outline：该轮廓不占用空间。外轮廓是属于一种动态样式，只有元素获取到焦点或者被激活时呈现。
	例：给input标签添加outline:none;可以取消该输入框获取焦点时的状态
	
	
12.CSS3中的filter滤镜效果（作用于图片上）：只有chrome的兼容性较好
		 -webkit-filter:grayscale(1);      //grayscale灰度,把图片变成灰色
		 -webkit-filter:sepia(1);			//实现旧照片的暗黄色效果
		 -webkit-filter:saturate(3);		//saturat是用来改变图片的饱和度(可超过100%)
		 -webkit-filter:invert(1);		//invert做出来的效果就像是我们照相机底面的效果一样
		 -webkit-filter:brightness(0.5);	//改变图片的亮度
		 -webkit-filter:contrast(2);		//改变图片的对比度
		 -webkit-filter:blur(3px);		//改变图片的清晰度,实现模糊效果
		 -webkit-filter:drop-shadow(5px 5px 5px #ccc);    	//类似box-shadow的效果，给图片添加阴影
		 
		 
13.z-index:在css2中z-index是需要和定位属性(position)一起使用的.