优化
<?php
	mysql优化
1.ip地址优化
	$int = ip2long('192.168.1.111');  //该函数将ip地址转换成数值。字段类型只需设置为int型，只占用4个字节空间，本身使用varchar格式需要占15个字节
	$ip = long2ip($int);   //该函数将ip地址数值转换为ip地址格式
	
2.避免使用select * 命令
	//从数据表中读取数据越多，查询速度越慢
	//即使要获取的数据为表中的所有数据，也不要使用*
	
3.不要使用order by rand() 命令
	//当需要从数据表中随机获取一条数据时，避免使用order by rand()，这样写会出现性能瓶颈。问题在于，MySQL可能会为表中每一个独立的行执行BY RAND()命令（这会消耗处理器的处理能力）
	//可以先查出所有数据的数量，然后使用mt_rand()函数随机获取一个数值，再进行查询

4.给需要检查的字段添加索引
	//给在sql语句中需要做为条件进行查询的字段建立索引，可以提高速度
	
5.使用limit 1取得唯一行
	//当需要查的数据仅一条时，可以增加limit 1提高查询效率，这样数据库引擎发现那条数据后将停止扫描，而不会去扫描整个表
	
6.查询缓存
	//尽量避免在where子句中使用or来进行连接，否则将导致引擎放弃使用索引而进行全表扫描
	//尽量避免在where子句中进行表达式操作，函数操作，使用不等号，这样会使查询不再使用索引
	//mysql服务器查询，默认会开启查询缓存。当同一个查询执行多次时，结果在缓存中提取速度相当快。
	//在sql语句中尽量避免使用可变的函数，使用可变的函数，会阻止查询缓存工作。
	
7.存储缓存
	//尽量避免在表中添加空值，尽量使用默认值代替
	
	php优化
//1.在类中，如果一个方法可静态化，就对他做静态声明，速率可提升四倍
//2.在使用for循环时，避免在参数中使用count等函数统计数量，因为如果使用count函数进行统计，每次循环都会统计一次数量


MYSQL相关使用：
	
1.mysql用户权限管理
	创建用户：	create user zx_root;
	修改用户名 ：rename user zx_root to zx;
	删除用户：drop user zx;		//同时会将该用户的相关权限一起删除
	设置用户密码：set password for zx =password('root');
	修改用户密码：update mysql.user set password=password('123') where user = 'zx';
	
	查看用户权限：show grants for zx;
	授予权限：grant select on mysql.user to zx;
	
2.mysql表数据，表结构复制
	复制表结构和数据：create table 新表 select * from 旧表	//新表中没有了旧表的primary key、Extra（auto_increment）等属性
	只复制表结构：select * into 新表 from 旧表 where 1=2;
	只复制表数据：select * into 新表 from 旧表;
	显示表的建表语句：show create table 表;
	
3.mysql索引的使用
	（1）创建索引的原则：
		1.建立唯一索引，可以进行快速定位
		2.为经常需要ORDER BY、GROUP BY、DISTINCT等操作的字段建立索引，可以有效的避免排序操作
		3.为经常做为查询条件的字段建立索引，可以提高查询速度
		4.限制索引的数目，每个索引都要占用磁盘空间，索引越多所占空间越大，同时更新表浪费的时间越长
		5.尽量使用数据量小的字段建立索引
		6.定期清除不再使用的索引
	（2）为表添加索引：
		alter table tablename add index emp_name (name);
	（3）删除表的索引：
		alter table tablename drop index emp_name;
	（4）重建mysql数据表索引：
		repair table tablename quick;	//可以提高索引效率
		
	
4.mysql删除字段：alter table tablename drop field_name;

5.mysql数据优化判断：
	在一个项目中，找到慢查询的select语句？
		mysql支持把慢查询语句记录到日志文件中（默认情况下慢查询记录是不开启的）。
		通过修改my.ini的配置文件，找到 [mysqld] 在其下面添加如下两行代码
		long_query_time=2	#设置慢查询的最低时间
		log-slow-queries=D:/mysql/logs/www.phpernote.com_slow_sql.log #设置把日志写在那里，可以为空，系统会给一个缺省的文件
		
		linux下开启慢查询： slow-query-log=1 #开启慢查询
							slow-query-log_file = /home/www/phpernote/mysql_slow_query.log #慢查询日志目录
							long_query_time = 1 #记录下查询时间超过1秒
		
	通过explain分析低效率的执行情况
		EXPLAIN SELECT * FROM order_copy WHERE id=12345
		select_type:表示查询的类型。  	尽量避免让type的结果为all
		Extra:执行情况的描述和说明		尽量避免让extra的结果为：using filesort
	
6.命令行下进行mysql数据的导入导出：
	导出数据某张表的全部数据：select * from m_picture into outfile 'd:/c.txt';
	导入数据到某张表中：load data infile 'd:/c.txt' into table m_picture;

7.表空间的释放：
	optimize table tablename	可以释放使用delete删除数据后剩余的碎片。执行过程中会进行锁表。
	
8.表自增字段重新计数：
	truncate table tablename;
	
9.mysql反应慢，cpu占用高的解决办法。
	1.修改my.ini中的tmp_table_size的大小
	2.show processlist 语句，查找负荷最重的 SQL 语句，优化该SQL	


10.limit优化：
	当使用limit的时候，偏移量offset较大的情况下使用子查询可以提高效率
					Select * From user Where uid >=( Select uid From user Order By uid limit 10000,1  ) limit 10;
	
	
mysql服务器的主从数据库同步：
	首先需要在同一个局域网内的两台机器（当然也可以用一台机器虚拟两台机器出来），都安装上mysql服务。
	主机A: 192.168.1.100
	从机B: 192.168.1.101
	1、先登录主机 A，执行如下命令赋予从机权限，如果有多台从机，就执行多次：
		mysql>GRANT REPLICATION SLAVE ON *.* TO 'backup'@'192.168.1.101' IDENTIFIED BY '123456';
	2、 打开主机A的my.cnf，输入如下配置参数：
		server-id = 1 #主机标示，整数
		log_bin = /var/log/mysql/mysql-bin.log #确保此文件可写
		read-only =0 #主机，读写都可以
		binlog-do-db =test #需要备份数据，多个写多行
		binlog-ignore-db =mysql #不需要备份的数据库，多个写多行
	3、打开从机B的my.cnf，输入如下配置参数：
		server-id = 2
		log_bin = /var/log/mysql/mysql-bin.log
		master-host =192.168.1.100
		master-user =backup
		master-pass =123456
		master-port =3306
		master-connect-retry=60 #如果从服务器发现主服务器断掉，重新连接的时间差(秒)
		replicate-do-db =test #只复制某个库
		replicate-ignore-db=mysql #不复制某个库
	4、同步数据库
		进过以上的配置，分别重启主机A和从机B，即可自动实现同步。
	
	
	
binary : 添加这个关键字可以让搜索条件区分大小写  例：select * from sysitem_sku where wgbez like binary '%c%';

重命名表：alert table client rename to clients;

distinct: 去除重复数据
	select distinct name from test;
	当对多个字段中的一个字段进行去重时：需要对去重的字段进行处理GROUP_CONCAT函数对字段进行处理，同时使用该字段做为分组字段
	select id,GROUP_CONCAT(DISTINCT title),group_id from documents GROUP BY title ORDER BY id;

GROUP_CONCAT函数：将某个字段数据在一行显示，配合group by使用   默认分割符为","
	参数group_concat([DISTINCT] 要连接的字段 [Order BY ASC/DESC 排序字段] [Separator '分隔符']) 
	
concat函数：字符串连接函数			CONCAT(str1,str2,...)  参数为要连接的字段，参数可自定义（例如）：可以使用"---"进行连接）

date_format  : 格式化日期时间  例：date_format(time,"%M %d,%Y");

CURDATE()函数：	获取当前日期
CURTIME()函数： 获取当前时间
NOW()函数： 返回当前日期和时间

LOWER(str)函数：	转换为小写
UPPER(str)函数：	转换为大写	

substring(str,start,len)函数：	对数据进行截取		参数：要截取的字段，开始位置（首个位置1），截取长度
left(str,len)函数：	参数：要截取的字段，截取的长度

ltrim(str)函数：	去掉数据左边的空白

REPLACE(str,str1,str2)函数：	参数：要替换的字符串，原字符串中要替换的字符，用来替换的字符

rand()函数：	获取0-1之间的随机数

WEEK(date[,mode])函数：  返回所给日期是一年中的第几周    	SELECT WEEK('1981-02-15');

IF(expr1,expr2,expr3)函数：如果expr1为真则返回expr2，否则返回expr3
	例子：select IF( salary > 2000, 'High', 'Low') from salary;
	