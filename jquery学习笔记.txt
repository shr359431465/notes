jQuery笔记：
	js中的this指的是一个HTML对象元素，jquery中的$(this)是一个jquery对象。
	jquery基本使用：
		$("#divid").click(function(){
			$(this).css({"background":"red"});
		});

	
	jQuery核心对象函数：
		each();//用来遍历jquery对象集合。
			例子：实现隔行换色。	i用来记录当前是第几个
				$("h1").each(function(i){
					if(i%2==0){
						$(this).css({"background":"#888"});
					}
				});
				
		size();//当前匹配的元素个数。   $("h1").size();
		
		get(index);//获取其中一个匹配元素。 index表示第几个元素
		
			js元素对象转成jquery对象：直接将通过js获取的对象放入jquery执行标记{$()}中。
				例子： var objs = document.getElementsByTagName("h1");
						   $(objs).css({"color":"blue"});	
				   
			jquery对象转换成js元素对象：$("h1")获取一个对象集合，通过get()方法取得其中的某个js元素对象。
				例子：alert($("h1").get(0).outerHTML);
			
		index(selector);//搜索匹配元素，返回相应元素的索引值(第几个元素)，从0开始。  selector指要搜索的选择器。
			例子：$("h1").click(function(){
					  alert($(this).index("h1"));
				  });
			实例：点击后使当前内容加上当前元素的索引值。
				  $("h1").click(function(){
					 var idx = $(this).index("h1");
					 $(this).text($(this).text() + idx);
				  });
				  
		data(key,value);//在标签身上隐藏一些与本标签有关的数据。key:存储的数据名。 value:存储的数据值。
			$("h1").each(function(i){
				$(this).data("idx","user");
			});
			$("h1").click(function(){
				alert($(this).data("idx"));
			});
		removeData(key);		//移除标签上的隐藏数据
	
	
	jQuery筛选元素：
		筛选：
			:nth-child;		//匹配父元素下第N个子元素。(该子元素必须含有父元素,序号从1开始)。$("ul li:nth-child(2)")
			eq(index);  //获取第N个元素，元素位置从0开始算起
			hasClass(class);  //查看当前元素是否含有某个特定的类。
			slice(start,end);   //获取从start开始到end结束的对象。不包含最后一个。 选取一个匹配的子集
			filter();        //过滤，找到含有指定类的对象。
			has();         //查找包含特定后代元素的标签。   $("li").has("ul").css({"background":"red"});
		
		查找：
			find();				//搜索所有与指定表达式匹配的元素			$("p").find("span");    //查找p标签下的span标签
			children();          //取得一个元素的所有子元素集合。				例子：通过点击第一个div中第二个h1，改变第二个div中第二个h1内容的颜色。
						$("div:eq(0)>h1:eq(1)").click(function(){
							$(this).parent().next().children().eq(1).css({"color":"green"});
						});
			
			parent();      		//找到一个元素的父元素。
			next();             //取得当前元素后面紧邻的兄弟元素。
			nextAll(); 			//取得当前元素后面所有的兄弟元素。
			prev();				//取得当前元素前面紧邻的兄弟元素。
			prevAll();			//取得当前元素前面所有的兄弟元素。
			siblings();			//取得当前元素前后所有的兄弟元素。
			andSelf();			//选择完一个元素后再加上自己。(加入先前所选元素。)  在1.8以上的版本中使用addBack()来代替    
			例子：通过点击第一个div中的第一个h1，改变其后两h1内容的颜色。
						$("div:eq(0)>h1(0)").click(function(){
							$(this).next().next().andSelf().css({"color":"red"});	
						});
	
	
	jQuery元素属性:
		attr();   //设置或获取属性值。    例子：获取属性值：$("img").attr("src");
												设置属性值：$("img").attr({"src":"1.jpg","alt":"这是一个图片"});
		removeAttr();      //删除一个属性。			$("input").removeAttr("disabled");
		addClass(class);        //为元素添加一个指定的类名。    $("p").addClass("tit");
		removeClass(class);		//从匹配到的元素中删除指定的类。	$("p").removeClass("tit");
		html();            //获取匹配元素的HTML内容。相当于js中的innerHTML.
				$("h1").eq(0).prop("outerHTML");   //该方法可以模拟js中的outerHTML方法
		text();				//取得匹配元素的内容。 可以对其从新赋值。  $("p").text("hello");
		val(val);             //获取匹配元素的当前值。一般只有在表单中才获取该值。    加上参数可以设置值。
		
		
	jQuery文档处理：
		//向每个匹配的元素内部追加内容。  (向后追加)
			append();          $("#div2").append($("#div1 h1"));   把div1中的所有h1移动到div2中。
			appendTo();        $("#div1 h1").appendTo($("div2"));
			
			实例：把左边选中内容移动至右边。
				<form style="display:inline">
					<select name="r" style="width:100px;height:150px;" multiple size="6">
						<option value="苹果">苹果</option>
						<option value="香蕉">香蕉</option>
						<option value="西瓜">西瓜</option>
						<option value="草莓">草莓</option>
					</select>
				</form>
				<button type="button">>></button>
				<button type="button"><<</button>
				<form style="display:inline">
					<select name="s" style="width:100px;height:150px;" multiple size="6">
					</select>
				</form>
				<script>
					$("button").eq(0).click(function(){
						$("form:eq(0) select option:selected").appendTo($("form:eq(1) select"));
					});
					$("button").eq(1).click(function(){
						$("form:eq(1) select option:selected").appendTo($("form:eq(0) select"));
					});
				</script>
		
		//向每个匹配的元素内部前置内容。（向前追加）
			prepend();
			prependTo();
			
			after();				//在每个匹配的元素之后插入内容。
			before();				//在每个匹配的元素之前插入内容。
			
			
			wrap();					//把匹配到的每一个元素用其他元素外包围。			$("h1").wrap("<i></i>");
			wrapAll();				//把匹配到的一个整体用其他元素包围。				$("h1").wrapAll("<i></i>");
			wrapInner();			//把匹配到的每一个元素的子元素用其他元素包围。		$("h1").wrapInner("<i></i>");
			
			replaceWith();       //用右边的替换左边的      $("p").replaceWith("<span>aaaaa</span>");
			empty();        	 //清除当前元素的子元素。
			remove();       	 //删除当前元素的所有内容。(包含该标签)
			clone();			//将匹配到的元素进行克隆	$("b").clone().prependTo("p");
	
	jQuery css样式函数：
		css();        		//设置或获取css样式。
		width();			//获取元素的宽度。
		height();			//获取元素高度。
		innerHeight();		//获取第一个匹配元素内部区域高度(包括补白，不包括边框)。
		outerHeight();		//获取第一个匹配元素外部高度（默认包括补白和边框）。
		scrollTop();		//获取滚动条滚动的高度。   $(window).scrollTop();
		offset();			//获取匹配元素的坐标（相对于坐标原点）。		$("img").offset().left;//获取横坐标。       	
																			$("img").offset().top;//获取纵坐标。
		position();			//获取匹配元素的坐标（相对于父元素）。父元素使用position:relative;定义。要获取的元素必须是使用top和left定位过的坐标。 
			例如：	img{
						position:absolute;
						top:100px;
						left:100px;
					}
		
		页面可视高度： $(window).height();
		页面总高度：	$(document).height();
		页面滚动的高度： $(window).scrollTop();
		
	jQuery事件：
		事件加载：		//当使用外部js时，所有代码包裹在$(function(){	})中，用来模拟页面dom元素加载完成后执行。
							$(function(){    //当dom加载完毕时。执行function中的代码
								$("h1").css({"color":"red","font-size":"40px"});
							});

		bind();   //为每个匹配元素的特定事件绑定事件处理函数。  例子：	function imgclk(){alert( $(this).attr("src") )}
																		$("img").bind("click",imgclk);
		one();		//为每个匹配元素的特定事件绑定一个一次性的事件处理函数。
		unbind();  //从每一个匹配的元素中删除绑定的事件。       例子：  $("img").unbind("click",imgclk);
		
		on();    //用来实现原来的live方法    例：$("#data_node").on('click','.delline',function(event){
													  $(this).closest('tr').remove();
													});
		closest();  //从本元素开始逐级向上匹配，并返回最先匹配的元素
		
		事件委派：  live();(已删除)     //为每个匹配元素的特定事件绑定事件处理函数。并且对后期新产生的相同匹配元素也有影响。是bind()的一个变体。
					die();      //用来解除live()事件。
		
		trigger();		//页面执行时，给匹配到的元素上添加触发事件
		
		unload();		//当离开页面时执行的事件。（谷歌内核的浏览器下无效）
						//用于谷歌内核的浏览器下：	$(window).on('beforeunload', function(){
													  return '确定要离开本页?';
													});
								
		
		事件切换：
			toggle():	//用于绑定。两个或多个事件处理器函数，以响应被选元素的轮流的 click 事件
				例子：  $("#divid").toggle(
							function(){
								$(this).css({"background":"pink","width":"500px"});
							},
							function(){
								$(this).css({"background":"#0ff","width":"300px"});
							}
						);
						
			hover();  	//用于响应被选元素被鼠标移入和移出事件。
		
		事件处理：
			change();      //当元素的值发生改变时，会发生 change 事件
						例子：实现文件上传内容显示功能。
			
			error();      //当加载失败时发生该事件。   $("img").error(function(){ $(this).attr({"src":"a.png"}) });
			
			load();        //载入一个html文件代码。获取另一个页面的数据。(该方法需要在服务器上进行使用)
			
	jQuery效果：
		show();         //显示   可以控制多久后显示，还可以控制显示后的其他事件(第二个参数用一个函数来写新的事件。)。
		hide();			//隐藏
		
		slideDown(); 	//通过高度变化（向下增大）来动态地显示所有匹配的元素         
															例子： $("#btn").click(function(){
																		$("#main").slideDown(5000,function(){
																			alert(1);
																		});
																	});
		slideUp();		//通过高度变化（向上减小）来动态地隐藏所有匹配的元素
		slideToggle();	//自动判断是隐藏还是显示。		
		
		fadeIn();       //通过不透明度的变化来实现所有匹配元素的淡入效果		透明度：opacity: 0.5;  
		fadeOut();		//通过不透明度的变化来实现所有匹配元素的淡出效果
		fadeTo();		//把匹配到的元素的透明度调整到指定透明度。     $("p").fadeTo(3000, 0.66);
																		//第一个参数为时间，第二个参数为透明度
		fadeToggle();		//自动判断淡入还是淡出
		
		delay();		//给动画效果设置延迟执行时间
		
	以上三对操作用法相同。
		animate();			//自定义动画效果。		$("#btn").click(function(){
														$("img").animate({"left":"700px"},1000).animate({"top":"400px"},1000);
													});
		stop();				//结束当前动画。
		
		
	jQuery ajax技术：
		实现和php页面数据传输：
			$.get();
			$.post();         	 例子：$("#btn").click(function(){                             		
											$.post("index.php",{"id":"1"},function(date){				//添加第四个参数可以直接将获取到的数据转为json对象。
												$("body").append(date);
											});
										});
								php页面使用json_encode($arr);可以将数组转为json字符串 。
			serialize();        //序列表单内容为字符串。			str = $("form").serialize();
			serializeArray();	//返回json对象

			ajaxStart();		//ajax请求开始时显示的信息				$("#loading").ajaxStart(function(){
																		   $(this).show();
																		 });
			ajaxStop();			//ajax结束后隐藏的信息
			ajaxSuccess();		//ajax请求成功时显示消息					
			
			
	event事件：
		event.which    	获取键盘码的方法
		event.pageX		获取点击点的距左边的距离
		event.pageY		获取点击点的距顶边的距离
		
	jquery工具方法：
		$.each();			用于遍历对象或数组		回调函数拥有两个参数：第一个为对象的成员或数组的索引，第二个为对应变量或内容
			例子：$.each( { name: "John", lang: "JS" }, function(i, n){
					  alert( "Name: " + i + ", Value: " + n );
					});
		
		$.trim(); 			//去掉起始和结尾的空格。			$.trim("  hello, how are you?  ");
			
		$.param();			//将json对象转换为一个key-value字符串。