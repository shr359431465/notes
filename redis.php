<?php
	windows下启动redis服务器:redis-server.exe redis.conf

	$redis->flushAll();   //清空整个redis
	$redis->flushDB();   //清空当前redis库
	$redis->del();   //删除一个或者多个key    当删除多个key时，里面传入的参数为数组array('first','second','thired');
	$redis->exists();    //判断key是否存在
	$redis->ttl();   //返回key的剩余存在时间（单位为秒）
	$redis->expire();	//设置key的生存周期（单位秒）
	$redis->sort();		//对列表，集合进行排序，返回一个递增的的排序结果
	
	$redis->set($key,$value);   //为key设置值（如果key已存在，新值会覆盖其中的旧值）   
	$redis->setnx();    //同上，当key已存在时，不做任何操作
	$redis->setex($key,$seconds,$value);   //在设置值得同时给该值设置存在时间（已存在会进行覆盖）
	$redis->mset();    //同时设置一个或多个key-value对。()
	$redis->mget(传一个数组);  //返回所有给定key的值，如果指定的key不存在，会返回特殊值（false）。因此该命令永不失效
	$redis->msetnx(传一个数组);  
			//同时设置一个或多个key-value对，当且仅当key不存在时才进行设置，即使只有一个key已存在，也会拒绝所有传入的设置操作。
	$redis->getset($key,$value);   //给指定的key设置值，同时返回该key的旧值，如果该key第一次设置值，则返回false
	$redis->strlen($key);   //返回key所储存的字符串值的长度
	$redis->incr($key);    //将key中储存的数字值增一
	$redis->decr($key);    //将key中储存的数字值减一
	$redis->incrby($key,数值);    //给key存储的数据增加值	
	$redis->decrby($key,数值);    //给key存储的数据减少值
	$redis->append('key','string');//把string追加到key现有的value中[追加后的个数]
	
//哈希表
	$redis->hset($key,$field,$value);   //给hash表中的key下的对象field添加value(覆盖操作)    	$redis->hset('demo','name',10);
	$redis->hsetnx();		//非覆盖操作			
	$redis->hmset('test',array('name'=>1,'name1'=>2,'name2'=>3));     //同时给test的多个对象设置值
	$redis->hget($key,$field);			//获取hash表中key下的对象field的值
	$redis->hmget('test',array('name','name1','name2'));    //获取test中多个对象的值，返回值是一个数组
	$redis->hgetall('test');    //同时获取test中的多个对象的值，返回值为一个数组
	$redis->hdel('test','name');		//删除test中的某个对象
	$redis->hlen('test');				//返回某个hash表中对象的数量（相当于count类似用法）
	$redis->hexists('test','name');     //查看hash表中给定的对象是否存在
	$redis->hkeys('test');				//获取hash表test的所有对象名（返回值是一个数组）
	$redis->hvals('test');				//获取hash表test的所有对象值（返回值是一个数组）


//列表
	$redis->lpush('test','name1');      //在列表test的左端（首端）插入value值（覆盖操作）(列表不存在会新建列表)
	$redis->lpushx('test','name1');		//（非覆盖操作）（列表不存在无法插入，不会默认新建列表）
	$redis->rpush('test','name1');      //在列表test的右端（末端）插入value值（覆盖操作）
	$redis->rpushx('test','name1');		//（非覆盖操作）
	$redis->lpop('test');				//删除列表的首端元素
	$redis->rpop('test');				//删除列表的末端元素
	$redis->lsize('test');				//获取列表的元素个数
	$redis->lget('test','index');				//获取test列表中index位置的元素的值    相同功能：$redis->lindex();
	$redis->lrange('test','start','end');		//获取test列表中start位置到end位置的值（end为-1时代表获取所有值）
	$redis->lgetrange();			//和上一个相同功能
	$redis->lrem('test','value','count');         //根据参数count的值，移除列表中与参数value相等的元素。
												//count的值可以是以下几种：
												//count > 0: 从表头开始向表尾搜索，移除与value相等的元素，数量为count。
												//count < 0: 从表尾开始向表头搜索，移除与value相等的元素，数量为count的绝对值。
												//count = 0: 移除表中所有与value相等的值。
	$redis->lset('test','index','value');        //将列表test中下标为index的值改为value
	$redis->ltrim('test',$start,$end);			//截取test列表中从start到end中的元素			
	
	
//集合操作
	$redis->sAdd("name","aa");			//向集合name中添加一个值
	$redis->sRem("name",$value);	-	//删除集合name中的指定value值
	$redis->sMembers("name",$value);	//返回名称为name的所有元素
	$redis->sInterStore("output",$key1,$key2,$key3);	//求三个集合的交集，并将交集保存在output集合中
	$redis->sUnionStore("output",$key1,$key2,$key3);	//求三个集合的并集，并将并集保存在output集合中
	$redis->sIsMember($key,$value);			//判断value是否存在于key集合中
	
//事物处理
	$redis->watch();         //监视一个(或多个) key ，如果在事务执行之前这个(或这些) key 被其他命令所改动，那么事务将被打断
	$redis->multi();		//标记一个事务块的开始
	$redis->exec();			//执行所有事务块内的命令
	$redis->discard();		//取消事务，放弃执行事务块内的所有命令