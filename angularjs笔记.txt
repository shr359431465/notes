angularjs笔记q

绑定services:	需要在控制器函数中加入定义的名称（如：realname）
	.value('realname','zhangsan');			//定义一个变量realname，其值可以改变
	
	.constant('http','www.baidu.com');		//定义一个变量http,其值不能改变
	
	.factory('Data',function(){				//定义一个工厂，return中的内容为该工厂的返回内容，
		return {										控制器中使用：	Data.setMsg();可以直接调用工厂中的方法
			msg:'你好',
			setMsg:function(){
				this.msg='我不好';
			}
		}
	})
	
	.service('User',function(){				//定义一个服务，相当于简化版的factory。
		this.firstname = 'aa';					在控制器中调用  User.getName();直接获取函数返回值
		this.lastname = 'bb';
		this.getName = function(){
			return this.firstname+this.lastname;
		}
	})
	
内置service服务：   将服务加入控制器函数中
	$location		$location.absUrl();//获取当前页面完整的url地址
					$location.path();//返回当前路径，带参数'/'意思是把路径修改为'/'路由
					$location.path('/home').replace();		//加replace()可以让跳转后，用户不能通过点击后退返回前一个地址
					$location.host();		//获取当前url的主机
	$http			$http.get("http://localhost/ng/ng.php").success(function(response){
						$scope.names = angular.fromJson(response);
						//console.log(res);
					});		//去拉取数据，并获取到响应值
	
使用ajax的方法提交数据： $http.post('process.php', $scope.formData)
							.success(function(data) {
								...
							});

							
指令：
	no-repeat='x in items'			//其中的变量：$index指x在数组中的下标    $first只有第一个下标返回true其余为false
																			 $last只有最后一个下标返回true其余为false
	
	$odd和$even代表单双数
	
自定义指令：
		app.directive("runoobDirective", function() {
			return {
				restrict : "A",							//A:runoob-directive做为一个属性	E:runoob-directive做为一个标签
				replace:true,							//指自定义directive的名称是否显示，默认为false，显示
				template : "<h1>自定义指令!</h1>",
				link:function(){						//当该指令调用是会自动调用该方法
					alert('hellow');
				}
			};
		});
	
解决跨域访问的问题：header("Access-Control-Allow-Origin: *");

ng-value='myVar'		//用于给input框设置值(使用ng-model可以代替相同功能)




子控制器可以访问父控制器中的$scope对象：app.controller('ParentController', function($scope) {
											  $scope.person = {greeted: false};
										});
										app.controller('ChildController', function($scope) {
											  $scope.sayHello = function() {
											  $scope.person.name = 'Ari Lerner';
										};
										});