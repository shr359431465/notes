<?php
	//优化
	1.当需要判断字符串长度是否大于某个值时，可以用isset代替strlen，可以提高执行效率
			if(!isset($str{5})) 比 if(strlen($str)>5) 更好
	
	
	
	//字符串函数
	一。可以代替正则判断
	1.ctype_alnum   检测是否是只包含[A-Za-z0-9]
	2.ctype_alpha   检测是否是只包含[A-Za-z]
	3.ctype_digit 	检查时候是只包含数字字符的字符串（0-9）
	4.ctype_lower 	检查是否所有的字符都是英文字母，并且都是小写的
	5.ctype_upper 	检查是否所有的字符都是英文字母，并且都是大写的
	
	二。对传递的get参数进行字符串编码
	1.urlencode($str)	对字符串进行编码
	2.urldecode($str)	解码已经编码后的字符串
	
	三。防止sql注入的方法
	1.htmlspecialchars		对提交的数据进行转义处理（将预定义字符转为html实体）
	2.htmlspecialchars_decode		对从数据库中获取到的转义后的html实体数据进行反转义
	3.addslashes		使用反斜线对预定义字符进行转义
	4.stripslashes		对使用addslashes转义后的字符进行反转义
	
	四。字符串函数
	1.str_pad 			对字符串两侧进行补白（默认为右填充）
	2.str_repeat		重复输出某个字符串
	3.str_split 		将一个字符串按照字符间距分割成数组
	4.strstr/stristr(不区分大小写)			查找字符串首次出现的位置（返回该位置及其之后的字符串。若第三个参数为true									，则返回该位置之前的字符串）
	
	五。防止模拟提交
	1.防止用户模拟ajax提交：若提交的数据来源于ajax提交，则在http头信息中会保存XMLHttpRequest来代表为ajax提交的数据
							通过$_SERVER['HTTP_X_REQUESTED_WITH']（只有提交方法为ajax提交时，才会有这个字段）获取到其中的值（XMLHttpRequest），进而可以验证数据的来源
	2.防止站外提交：可以通过获取$_SERVER['HTTP_REFERER']（其中保存的数据为数据来源地址）的值来判断数据来源
	
	3.获取访问者ip:function getIP(){//记录来访者的IP信息
						if(getenv('HTTP_CLIENT_IP')){
							$ip=getenv('HTTP_CLIENT_IP');
						}else if(getenv('HTTP_X_FORWARDED_FOR')){
							$ip=getenv('HTTP_X_FORWARDED_FOR');
						}else if(getenv('REMOTE_ADDR')){
							$ip=getenv('REMOTE_ADDR');
						}else{
							$ip=$_SERVER['REMOTE_ADDR'];
						}
						return $ip;
					}
	
	六。php函数（数组和对象相关）
	1.php对象转数组       get_object_vars(); //返回由对象属性组成的关联数组
	2.把数组中的一部分去掉并用其它值取代    array_splice();  //第四个参数为用来替换的数据
	3.过滤数组中的单元		array_filter();		//可以用来过滤数组中的空数据
	
	七。相关实例接口
	1.获取qq用户的头像：<img src="'.'http://q1.qlogo.cn/g?b=qq&nk='.$qq.'&s=100&t='.time().'">
	
	
数组排序：
	array_multisort($order,SORT_ASC,SORT_NUMERIC,$cates); 参数：用于控制排序的字段数组，排序方式，数据比较类型，待排序的数组
	
	array_reverse();使数组反序排列